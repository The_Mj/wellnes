from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
# Create your views here.
# def profile(request):
#     return render(request , 'accounts/profile.html')

def register(request):
    if request.method == 'POST':

        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()

            return redirect('/')
    else:
        form = UserCreationForm()

    return render(request, 'registration/signup.html', {'form' : form})

