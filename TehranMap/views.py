from django.shortcuts import render , redirect
from django.http import HttpResponse

from django.views.generic import TemplateView

from .models import Club
from .models import Group

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
# Create your views here.

# def index(request):
#     return render(request, 'index.html')

@login_required
def list(request ,region_id , group_id):
    club = Club.objects.filter(region=region_id).filter(group_id=group_id)
    # club_group = Group.objects.get(group_id=club.name)
    return render(request, 'list.html', {'clubs':club})


def map(request):
    club = Club.objects.all()
    return render(request, 'map/map.html', {'club':club})


# def signup(request):
#     if request.user.is_authenticated:
#         return redirect('/')
#     if request.method == 'POST':
#         form = UserCreationForm(request.POST)
#         if form.is_valid():
#             form.save()
#             username = form.cleaned_data.get('username')
#             raw_password = form.cleaned_data.get('password1')
#             user = authenticate(username=username, password=raw_password)
#             login(request, user)
#             return redirect('/')
#     else:
#
#         form = UserCreationForm()
#
#         return render(request, 'registration/signup.html', {'form': form})


#
# class CompelexMenu(TemplateView):
#
#     template_name='menu.html'
#
#
# def get_context_data(self, **kwargs):
#     # Call the base implementation first to get a context
#     context = super().get_context_data(**kwargs)
#     # Add in a QuerySet of all the books
#     context['clubs'] = Club.objects.get(region=region_id)
#     return context

def CompelexMenu(request, region_id ):
    # clubs= Club.objects.filter(region=region_id)

    return render(request , 'menu.html' , {'region': region_id })

