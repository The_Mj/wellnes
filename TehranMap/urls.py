from django.urls import include, path
from . import views
from django.conf.urls import url
app_name='tehranMap'
urlpatterns = [
    path('compelexes/', views.map , name='map'),
    path('compelexes/<int:region_id>', views.CompelexMenu , name='menu'),
    path('compelexes/<int:region_id>/<int:group_id>', views.list , name='list'),
    # url(r'^register/$', views.signup, name='register'),

]