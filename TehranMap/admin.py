from django.contrib import admin

from TehranMap.models import (
    Club,
    Group,
)
# Register your models here.

@admin.register(Club)
class ClubAdmin(admin.ModelAdmin):
    model = Club
    list_display = ('name', 'address', 'phone' , 'region')
    list_filter = ('name' , 'address', 'phone' , 'region')
    # pass

@admin.register(Group)
class GropuAdmin(admin.ModelAdmin):
    model = Club
    list_display = ('name', )
    list_filter = ('name', )
    # pass