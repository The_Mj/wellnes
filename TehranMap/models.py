from django.db import models

# Create your models here.

class Group(models.Model):
    name=models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Club(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=300)
    phone = models.CharField(max_length=8)
    region = models.CharField(max_length=2 , null=True)
    group = models.ForeignKey(Group , on_delete=models.SET_NULL , null=True , blank=True)
    def __str__(self):
        return self.name