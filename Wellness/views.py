from django.shortcuts import render , redirect
from django.http import HttpResponse




from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
# Create your views here.

def index(request):

    return render(request, 'index.html')